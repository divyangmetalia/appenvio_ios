//
//  SliderVC.m
//  Employee
//
//  Created by Elluminati - macbook on 19/05/14.
//  Copyright (c) 2014 Elluminati MacBook Pro 1. All rights reserved.
//

#import "SliderVC.h"
#import "Constants.h"
#import "SWRevealViewController.h"
#import "PickUpVC.h"
#import "CellSlider.h"
#import "HistoryVC.h"
#import "AboutVC.h"
#import "PaymentVC.h"
#import "ProfileVC.h"
#import "PromotionsVC.h"
#import "ContactUsVC.h"
#import "UIView+Utils.h"
#import "UIImageView+Download.h"
#import "UberStyleGuide.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "CustomAlert.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface SliderVC ()
{
    NSMutableArray *arrListName,*arrIdentifire;
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
    NSString *strContent,*strShareLink;
}

@end

@implementation SliderVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tblMenu.backgroundView=nil;
    self.tblMenu.backgroundColor=[UIColor clearColor];
    [self.imgProfilePic applyRoundedCornersFullWithColor:[UIColor clearColor]];
    
    arrSlider=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"Profile", nil),NSLocalizedString(@"Booking", nil),NSLocalizedString(@"History", nil),NSLocalizedString(@"Payment", nil),nil ];
    arrImages=[[NSMutableArray alloc]initWithObjects:@"nav_profile",@"nav_profile",@"ub__nav_history",@"nav_payment",nil];
    arrIdentifire=[[NSMutableArray alloc]initWithObjects:SEGUE_PROFILE,SEGUE_TO_BOOKING,SEGUE_TO_HISTORY,SEGUE_PAYMENT, nil];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    NSMutableArray *arrImg=[[NSMutableArray alloc]init];
    for (int i=0; i<arrPage.count; i++)
    {
        NSMutableDictionary *temp1=[arrPage objectAtIndex:i];
        [arrTemp addObject:[NSString stringWithFormat:@"  %@",[temp1 valueForKey:@"title"]]];
        [arrImg addObject:@"nav_about"];
    }

    [arrSlider addObjectsFromArray:arrTemp];
    [arrIdentifire addObjectsFromArray:arrTemp];
    [arrIdentifire addObject:SEGUE_TO_REFERRAL_CODE];
    //[arrIdentifire addObject:@"segueToShareTrack"];

    [arrImages addObjectsFromArray:arrImg];
    
    [arrSlider addObject:NSLocalizedString(@"Referral", nil)];
    [arrImages addObject:@"nav_referral"];
    
    [arrSlider addObject:NSLocalizedString(@"Share Track", nil)];
    [arrImages addObject:@"nav_payment"];

    [arrSlider addObject:NSLocalizedString(@"Logout", nil)];
    [arrImages addObject:@"ub__nav_logout"];
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [self.viewForShareTrack addGestureRecognizer:singleTapGestureRecognizer];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.viewForShareTrack.hidden=YES;
    NSMutableDictionary *dictInfo=[PREF objectForKey:PREF_LOGIN_OBJECT];
    [self.imgProfilePic downloadFromURL:[dictInfo valueForKey:@"picture"] withPlaceholder:nil];
    self.lblName.text=[NSString stringWithFormat:@"%@ %@",[dictInfo valueForKey:@"first_name"],[dictInfo valueForKey:@"last_name"]];
    
    self.tblMenu.tableFooterView=self.footerView;
    UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
    frontVC=[nav.childViewControllers objectAtIndex:0];
}

-(void)handleTap
{
    self.viewForShareTrack.hidden=YES;
}

#pragma mark -
#pragma mark - UITableView Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrSlider count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellSlider *cell=(CellSlider *)[tableView dequeueReusableCellWithIdentifier:@"CellSlider"];
    if (cell==nil) {
        cell=[[CellSlider alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellSlider"];
    }
    cell.lblName.font=[UberStyleGuide fontRegular:15.43f];
    cell.lblName.text=[arrSlider objectAtIndex:indexPath.row];
    cell.imgIcon.image=[UIImage imageNamed:[arrImages objectAtIndex:indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if ([[arrSlider objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"Logout", nil)])
    {
        CustomAlert *alert = [[CustomAlert alloc] initWithTitle:NSLocalizedString(@"Logout", nil) message:NSLocalizedString(@"Logout Msg", nil) delegate:self cancelButtonTitle:@"" otherButtonTitle:@""];
        [APPDELEGATE.window addSubview:alert];
        [APPDELEGATE.window bringSubviewToFront:alert];
        [self.revealViewController rightRevealToggle:self];
        return;
    }
    if([[arrSlider objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"Share Track", nil)])
    {
        [self onClickShareTrack:nil];
        //[self.revealViewController rightRevealToggle:self];
        return;
    }
    
    if ((indexPath.row >3)&&(indexPath.row<(arrSlider.count-3)))
    {
        [self.revealViewController rightRevealToggle:self];
        
        UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
        
        self.ViewObj=(PickUpVC *)[nav.childViewControllers objectAtIndex:0];
        
        NSDictionary *dictTemp=[arrPage objectAtIndex:indexPath.row-4];
        strContent=[dictTemp valueForKey:@"content"];
        
        [self.ViewObj performSegueWithIdentifier:@"contactus" sender:dictTemp];
        return;
    }
    
    [self.revealViewController rightRevealToggle:self];
    UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
    self.ViewObj=(PickUpVC *)[nav.childViewControllers objectAtIndex:0];
    if(self.ViewObj!=nil)
        [self.ViewObj goToSetting:[arrIdentifire objectAtIndex:indexPath.row]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 1)];
    v.backgroundColor=[UIColor clearColor];
    return nil;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - 
#pragma mark- UIButton Action methods

- (void)onClickProfile:(id)sender
{
    if (frontVC) {
        [self.revealViewController rightRevealToggle:nil];
        [frontVC performSegueWithIdentifier:SEGUE_PROFILE sender:frontVC];
    }
}

- (void)onClickPayment:(id)sender
{
    if (frontVC) {
        [self.revealViewController rightRevealToggle:nil];
        [frontVC performSegueWithIdentifier:SEGUE_PAYMENT sender:frontVC];
    }
}

- (void)onClickPromotions:(id)sender
{
    if (frontVC) {
        [self.revealViewController rightRevealToggle:nil];
        [frontVC performSegueWithIdentifier:SEGUE_PROMOTIONS sender:frontVC];
    }
}

- (void)onClickShare:(id)sender
{
    if (frontVC) {
        [self.revealViewController rightRevealToggle:nil];
        [frontVC performSegueWithIdentifier:SEGUE_SHARE sender:frontVC];
    }
}

- (void)onClickSupport:(id)sender
{
    if (frontVC) {
        [self.revealViewController rightRevealToggle:nil];
        [frontVC performSegueWithIdentifier:SEGUE_SUPPORT sender:frontVC];
    }
}

- (void)onClickAbout:(id)sender
{
    if (frontVC) {
        [self.revealViewController rightRevealToggle:nil];
        [frontVC performSegueWithIdentifier:SEGUE_ABOUT sender:frontVC];
    }
}

- (IBAction)onClickShareTrack:(id)sender
{
    NSString *strRequestId = [PREF objectForKey:PREF_REQ_ID];
    
    if([strRequestId intValue]!=-1)
    {
            if([[AppDelegate sharedAppDelegate]connected])
            {
                [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
                NSString *strForUserId=[PREF objectForKey:PREF_USER_ID];
                NSString *strForUserToken=[PREF objectForKey:PREF_USER_TOKEN];
                
                NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                
                [dictParam setValue:strForUserId forKey:PARAM_ID];
                [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
                [dictParam setValue:strRequestId forKey:PARAM_REQUEST_ID];
                                
                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                [afn getDataFromPath:FILE_SHARE_TRACK withParamData:dictParam withBlock:^(id response, NSError *error)
                 {
                     if (response)
                     {
                         if([[response valueForKey:@"success"]boolValue])
                         {
                             //[APPDELEGATE showToastMessage:NSLocalizedString(@"MAIL_SEND", nil)];
                             strShareLink = [response valueForKey:@"link"];
                             
                             NSLog(@"shareButton pressed");
                             
                             NSString *texttoshare = strShareLink; //this is your text string to share
                             //UIImage *imagetoshare = @""; //this is your image to share
                             NSArray *activityItems = @[texttoshare];
                             
                             UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
                             activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
                             [self presentViewController:activityVC animated:TRUE completion:nil];
                         }
                         else
                         {
                             [[AppDelegate sharedAppDelegate]hideLoadingView];
                             
                             NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                             NSString *str1=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                             if([str1 isEqualToString:@"405"])
                             {
                                 [self performSegueWithIdentifier:SEGUE_UNWIND sender:self];
                             }
                             else
                             {
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[afn getErrorMessage:str] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 [alert show];
                             }
                         }
                         
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                     }
                 }];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                [alert show];
            }
    }
    else
    {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_SHARE_TRACK", nil)];
    }
    
}

#pragma mark custome Alert view :
#pragma mark - Alert Button Clicked Event


- (void)customAlertView:(CustomAlert*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex==0)
    {
        NSLog(@"cancel ");
    }
    else{
        NSLog(@"done ");

        [PREF removeObjectForKey:PREF_USER_TOKEN];
        [PREF removeObjectForKey:PREF_REQ_ID];
        [PREF removeObjectForKey:PREF_IS_LOGOUT];
        [PREF removeObjectForKey:PREF_USER_ID];
        [PREF removeObjectForKey:PREF_IS_LOGIN];
        [PREF synchronize];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}


#pragma mark -alertview 

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100)
    {
        if (buttonIndex == 1)
        {
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:[PREF objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictParam setObject:[PREF objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            
            if([[AppDelegate sharedAppDelegate]connected])
            {
                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                [afn getDataFromPath:FILE_LOGOUT withParamData:dictParam withBlock:^(id response, NSError *error)
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     if (response)
                     {
                         [PREF removeObjectForKey:PREF_USER_TOKEN];
                         [PREF removeObjectForKey:PREF_REQ_ID];
                         [PREF removeObjectForKey:PREF_IS_LOGOUT];
                         [PREF removeObjectForKey:PREF_USER_ID];
                         [PREF removeObjectForKey:PREF_IS_LOGIN];
                         [PREF synchronize];
                         FBSDKLoginManager *logout = [[FBSDKLoginManager alloc] init];
                         [logout logOut];
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                 }];
            }
        }
    }
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
